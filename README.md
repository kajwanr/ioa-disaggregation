# IOA disaggregation

Code developed for IOA course project. 
The main purpose of the code is to automatically disaggregate a company from EXIOBASE using transactions data from the company, and then calculate its environmental footprint.

To run the code, three things are needed:
1. A financial transactions account for the company. See `/data/raw/company/base_scenario/transactions.xlsx` for a template.
2. A stressor account for the company. See `/data/raw/company/base_scenario/stressors.xlsx` for a template.
3. A EXIOBASE data file in *.mat* format (e.g. *IOT_2016_ixi.mat*). Preferably placed in `/data/raw/exio/`.

The code is run by running the scripts in `/src/scripts/` in the order indicated by their numbers. 

The code has only been tested on **MatLab version 9.8.0.1380330 (R2020a) Update 2.**




