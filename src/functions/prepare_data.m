function prepare_data(scenario, rest_limit_fraction, exio_path)
% 
fprintf("Starting data preparation for %s.\n", scenario)
company.scenario = scenario;
company.rest_limit_fraction = rest_limit_fraction;
company.exio_path = exio_path;
company.transactions_path = "data\raw\company\" + company.scenario + "\transactions.xlsx";
company.stressors_path = "data\raw\company\" + company.scenario + "\stressors.xlsx";

% Load data.
load(company.exio_path);
company.table = readtable(company.transactions_path);
company.stressor_factors = readtable(company.stressors_path);

% Calculate flow matrix, Z, for IO model.
IO.Z = IO.A * diag(IO.x);

% Prepare meta data for company based on transaction information.
% Make an overview of company industries (departments), regions and their parent industry/region.

% Assume that all company departments has atleast one transaction to it
% from the rest of the economy.
company_table_to = table( ...
    company.table.to_industry, ...
    company.table.to_region ...
);

% Get indexes, i, of the unique industry/region pairs.
[~, i] = unique(company_table_to);

% Filter rows, so only transactions to unique industry/region pairs exist.
filtered_table_1 = company.table(i, :);

% All non-company industries are integers (exiobase industries). 
% Use this to filter out those rows.
filter = isnan(str2double(filtered_table_1.to_industry));
filtered_table_2 = filtered_table_1(filter, :);

% Create columns for the new table.
company_industry = filtered_table_2.to_industry;
company_region = filtered_table_2.to_region;
parent_industry = filtered_table_2.parent_industry;
parent_region = filtered_table_2.parent_region;

% Compile to a table.
company.industries = table( ...
    company_industry, ...
    company_region, ...
    parent_industry, ...
    parent_region ... 
);

% Get dimensions and make placeholders for the Z matrix. 
% Dimensions of company.
company.Dims = size(company.industries, 1);

% Make placeholders for adjusted matrices/vectors.
company.Z_Revenue = sparse(zeros(company.Dims, meta.Zdim));
company.Z_Expense = sparse(zeros(meta.Zdim, company.Dims));
company.Z_Internal = sparse(zeros(company.Dims, company.Dims));
company.company_V = sparse(zeros(meta.Vdim, company.Dims, "double"));
company.company_S = sparse(zeros(meta.Fdim, company.Dims));
company.company_F = sparse(zeros(meta.Fdim, company.Dims));

% Make a copy of the IO model vectors and matrices that needs to be adjusted.
% The rest Z is used to calculate the transactions from other industries. 
% Company can insert a "zero-transaction" table if they know, that there should be no purchases from specific industries. 
company.Z = IO.Z;
restZ = company.Z;
company.V = IO.V;
company.S = sparse(IO.S);
company.F = sparse(IO.F);

fprintf("Disaggregating Z.\n")
% Disaggregation of transaction matrix, Z.
% Get size of transactions, and loop over them.
table_size = size(company.table, 1);

for index = 1:table_size
    row = company.table(index, :);
    
    % Get MRIO index of "from" industry/region.
    % TODO: Turn below into a function.
    region_map = ismember(meta.secLabsZ.Country, row.from_region);
    industry_map = ismember(meta.secLabsZ.Number, str2double(row.from_industry{1, 1}));
    map = logical(region_map .* industry_map);
    from_index = meta.secLabsZ(map, :).mrNumber;
    
    % Get MRIO index of "to" industry/region.
    region_map = ismember(meta.secLabsZ.Country, row.to_region);
    industry_map = ismember(meta.secLabsZ.Number, str2double(row.to_industry{1, 1}));
    map = logical(region_map .* industry_map);
    to_index = meta.secLabsZ(map, :).mrNumber;
    
    % Get MRIO index of "parent" industry/region.
    region_map = ismember(meta.secLabsZ.Country, row.parent_region);
    industry_map = ismember(meta.secLabsZ.Number, row.parent_industry);
    map = logical(region_map .* industry_map);
    parent_index = meta.secLabsZ(map, :).mrNumber;
    
    
    % Handle "Rest" purchases differently.
    if strcmp(row.from_industry, "Rest")
        continue
    end
    
    % Handle the three possible cases.
    switch row.type{1, 1}
        case 'Expense'
            % Locate where in MRIO the value needs to be subtracted.
            row_index = from_index;
            column_index = parent_index;
            
            % Ensure no negative flow values after subtraction.
            assert(company.Z(row_index, column_index) - row.value > 0, "Negative values!")
            
            % Do subtraction.
            company.Z(row_index, column_index) = company.Z(row_index, column_index) - row.value;
            
            % Get index of where in the company the transaction goes to.
            to_industry_index = strcmp(company.industries.company_industry, row.to_industry);
            to_region_index = strcmp(company.industries.company_region, row.to_region);
            company_to_index = logical(to_industry_index .* to_region_index);
            
            % Insert expense. 
            company.Z_Expense(row_index, company_to_index) = row.value;
            
            % Set value of MRIO transactions to zero. 
            % This ensures that this value is not used in the 
            % rest transaction calculation.
            restZ(row_index, column_index) = 0;
            
        case 'Revenue'
            % Locate where in MRIO the value needs to be subtracted.
            row_index = parent_index;
            column_index = to_index;
            
            % Ensure no negative flow values
            assert(company.Z(row_index, column_index) - row.value > 0, "Negative values!")
            
            % Do subtraction.
            company.Z(row_index, column_index) = company.Z(row_index, column_index) - row.value;
            
            % Get index of where in the company the transaction goes from.
            from_industry_index = strcmp(company.industries.company_industry, row.from_industry);
            from_region_index = strcmp(company.industries.company_region, row.from_region);
            company_from_index = logical(from_industry_index .* from_region_index);
            
            % Insert revenue. 
            company.Z_Revenue(company_from_index, column_index) = row.value;
            
        case 'Internal'   
            % Get index of where in the company the transaction goes to.
            to_industry_index = strcmp(company.industries.company_industry, row.to_industry);
            to_region_index = strcmp(company.industries.company_region, row.to_region);
            company_to_index = logical(to_industry_index .* to_region_index);
            
            % Get index of where in the company the transaction goes from.
            from_industry_index = strcmp(company.industries.company_industry, row.from_industry);
            from_region_index = strcmp(company.industries.company_region, row.from_region);
            company_from_index = logical(from_industry_index .* from_region_index);
            
            % Insert internal transaction.
            company.Z_Internal(company_from_index, company_to_index) = row.value;
            
            % ? Should we subtract internal trade from orginal Z matrix?
            % If we don't, it leads to an increase in total Z.
            
            % Get column index of the parent industry of transaction. 
            to_parent_industry = company.industries.parent_industry(company_to_index, :);
            to_parent_region = company.industries.parent_region(company_to_index, :);
            
            % Get row index of the parent industry of transaction.
            from_parent_industry = company.industries.parent_industry(company_from_index, :);
            from_parent_region = company.industries.parent_region(company_from_index, :);
            
            % Remove "if"-clause if we should subtract internal trade
            % within a country and not just international-internal trade.
            if ~strcmp(to_parent_region, from_parent_region)
            
                region_map = ismember(meta.secLabsZ.Country, from_parent_region);
                industry_map = ismember(meta.secLabsZ.Number, from_parent_industry);
                map = logical(region_map .* industry_map);
                from_index = meta.secLabsZ(map, :).mrNumber;
                
                region_map = ismember(meta.secLabsZ.Country, to_parent_region);
                industry_map = ismember(meta.secLabsZ.Number, to_parent_industry);
                map = logical(region_map .* industry_map);
                to_index = meta.secLabsZ(map, :).mrNumber;
                
                company.Z(from_index, to_index) = company.Z(from_index, to_index) - row.value;
            end 
        otherwise
            fprintf('Invalid transaction type. \n')
    end
end

% Now handle rest cases.
rest_table = company.table(strcmp(company.table.from_industry, "Rest"), :);

for index = 1:size(rest_table, 1)
    row = rest_table(index, :);
    
    % Get column index in MRIO that represents the parent industry/region.
    region_map = ismember(meta.secLabsZ.Country, row.parent_region);
    industry_map = ismember(meta.secLabsZ.Number, row.parent_industry);
    map = logical(region_map .* industry_map);
    parent_index = meta.secLabsZ(map, :).mrNumber;
    
    % Get index of where in the company the transaction goes to.
    to_industry_index = strcmp(company.industries.company_industry, row.to_industry);
    to_region_index = strcmp(company.industries.company_region, row.to_region);
    company_to_index = logical(to_industry_index .* to_region_index);
    
    % Take column of parent industry from the restZ matrix.
    % Note here that all rows in this vector, where transactions
    % already has been taken from (i.e. represented in the transactions
    % table) are set to zero!
    parent_column = restZ(:, parent_index);
    
    % Normalise it.
    normalised_parent_column = parent_column / sum(parent_column);
    
    % Use the rest_limit_fraction variable to decide which rows
    % are not of interest. I.e. rows where transactions are less than
    % 1 % of total transactions to parent industry. 
    drop_rows = (normalised_parent_column < company.rest_limit_fraction);
    
    % Set all other rows to zero
    normalised_parent_column(drop_rows) = 0;
    
    % Renormalize industries of interest
    renormalised_parent_column = normalised_parent_column / sum(normalised_parent_column);
    
    % Multiply with transaction value.
    rest_column = renormalised_parent_column * row.value;

    % Insert into company.Z_Expense.
    company.Z_Expense(:, company_to_index) = rest_column;
    
    % Subtract from Z.
    company.Z(:, parent_index) = company.Z(:, parent_index) - rest_column;
    
end

fprintf("Disaggregating V.\n")
% Disaggregation of value added, V.
% Get total input (missing value added) for company industries.
vertical_sum = sum(company.Z_Internal, 1) + sum(company.Z_Expense, 1);

% Get total output for company industries.
company.company_X = sum(company.Z_Internal, 2) + sum(company.Z_Revenue, 2);

% Total value added is the difference between these two.
company.company_V_total = company.company_X' - vertical_sum;

% Loop over each industry/region pair in company.
for index = 1:company.Dims
    row = company.industries(index, :);
    
    % Get index in MRIO that represents the parent industry/region.
    region_map = ismember(meta.secLabsZ.Country, row.parent_region);
    industry_map = ismember(meta.secLabsZ.Number, row.parent_industry);
    map = logical(region_map .* industry_map);
    parent_index = meta.secLabsZ(map, :).mrNumber;
    
    % Create proxy for the value added, i.e. what fraction of the total
    % value added belongs to each value added row.
    proxy = company.V(:, parent_index) / sum(company.V(:, parent_index));
    
    % Multiply total with proxy to get each value added row.
    company.company_V(:, index) = proxy * company.company_V_total(:, index);
    
    % Substract the amount from the parent industry.
    company.V(:, parent_index) = company.V(:, parent_index) - company.company_V(:, index);
    
end

% Calculate new total output.
company.X = sum(company.Z, 1) + sum(company.Z_Revenue, 1) + sum(company.V, 1);

fprintf("Disaggregating S and F.\n")
% Disaggregation of emissions, S and F.
% Get a list of all unique stressors of interest.
stressors_unique = unique(company.stressor_factors.stressor_index);

% Loop over all stressors.
for index_i = 1:size(stressors_unique, 1)
    stressor = stressors_unique(index_i, :);
    
    % Get the factors for this specific stressor.
    stressor_filter = (company.stressor_factors.stressor_index == stressor);
    stressor_table = company.stressor_factors(stressor_filter, :);
    
    % Make placeholder for each parent stressor index.
    % This is used to figure out which stressor values, S,
    % needs to be recalculated after procedure.
    parent_indexes = [];
    
    % Loop over each company industry/region pair.
    for index_j = 1:company.Dims
        % Get information about company industry (department).
        industry = company.industries(index_j, :);
        % Get total output of that company industry.
        industry_X = company.company_X(index_j);
        
        % Get parent index of that company industry.
        region_map = ismember(meta.secLabsZ.Country, industry.parent_region);
        industry_map = ismember(meta.secLabsZ.Number, industry.parent_industry);
        map = logical(region_map .* industry_map);
        parent_index = meta.secLabsZ(map, :).mrNumber;
        
        % Get index of company industry/region pair and get its
        % corresponding information in the stressor table.
        filter_industry = strcmp(stressor_table.company_industry, industry.company_industry);
        filter_region = strcmp(stressor_table.company_region, industry.company_region);
        filter = logical(filter_industry .* filter_region);
        industry_stressor_row = stressor_table(filter, :);
        
        % Mulitply with the ratio factor with the stressor factor of parent
        % industry.
        company.company_S(stressor, index_j) = IO.S(stressor, parent_index) *  industry_stressor_row.ratio_factor;

        % Multiply total output of company industry with new stressor for
        % that company industyr.
        company.company_F(stressor, index_j) = company.company_S(stressor, index_j) * industry_X;
        
        % Adjust total emissions, F.
        company.F(stressor, parent_index) = company.F(stressor, parent_index) - company.company_F(stressor, index_j);
        
        % Add parent index for later adjustment.
        parent_indexes(end+1) = parent_index;
    end
    
    % Re-adjust S for parent industries.
    unique_parent_indexes = unique(parent_indexes);
    for index_k = 1:size(unique_parent_indexes, 1)
        parent_index = unique_parent_indexes(index_k);
        company.S(stressor, parent_index) = company.F(stressor, parent_index) / company.X(parent_index);
    end
end

fprintf("Disaggregating Y.\n")
% Expansion of Y
company.company_Y = sparse(zeros(company.Dims, meta.Ydim));
company.Y = IO.Y;

fprintf("Combining and savings results.\n")
% Combine
company_combined.Z = [company.Z, company.Z_Expense; company.Z_Revenue, company.Z_Internal];
company_combined.S = [company.S, company.company_S];
company_combined.V = [company.V, company.company_V];
company_combined.Y = [company.Y; company.company_Y];
company_combined.x = [company.X, company.company_X'];
company_combined.F = [company.F, company.company_F];
company_combined.VY = IO.VY;

clearvars -except company IO meta company_combined

% Save
IO.F = sparse(IO.F);
IO.S = sparse(IO.S);
IO.F_hh = sparse(IO.F_hh);

warning('off', 'MATLAB:MKDIR:DirectoryExists');
mkdir("data\interim\" + company.scenario)
save("data\interim\" + company.scenario + "\data.mat", '-v7.3');
fprintf("Results succesfully saved for %s at path: data\\interim\\%s\\data.mat \n \n", company.scenario, company.scenario)
end

