function [results] = run_analysis(scenario, impact_name)
%RUN_ANALYSIS Summary of this function goes here
%   Detailed explanation goes here
fprintf("Starting analysis for %s.\n", scenario)

load("data\interim\" + scenario + "\data.mat")

% Prepare variables
impact_index = strcmp(impact_name, meta.labsC);
company_combined.x(company_combined.x == 0) = 1e-6;
company_combined.A = company_combined.Z / diag(company_combined.x);
company_combined.L = inv(company_combined.A^0 - company_combined.A);
company_combined.B = diag(company_combined.x) \ company_combined.Z;
company_combined.G = inv(company_combined.B^0 - company_combined.B);

fprintf("Calculating direct emissions.\n")
% Direct emissions (GWP)
impact = IO.char * company.company_F;
direct_impact = impact(impact_index, :);

fprintf("Calculating upstream emissions.\n")
% Upstream emissions (GWP)
company_Y = zeros(size(company_combined.Y, 1), company.Dims);
company_Y(meta.Zdim+1:meta.Zdim+company.Dims, :) = diag(sum(company.Z_Revenue, 2) + sum(company.Z_Internal, 2));
company_x = company_combined.L * company_Y;
ratios = diag(company_Y(7987+1:7987+company.Dims, :)) ./ diag(company_x(7987+1:7987+company.Dims, :));
ratios = repmat(ratios', meta.Zdim + company.Dims, 1);
company_x_adjusted = ratios .* company_x;
impact = IO.char * company_combined.S * company_x_adjusted;
upstream_impact = impact(impact_index, :);

fprintf("Calculating downstream emissions.\n")
% Downstream emissions (GWP)
company_combined.V_total = sum(company_combined.V, 1);
downstream_allocation = diag(company_combined.V_total) * company_combined.G;
emissions = downstream_allocation * company_combined.S';
impact = IO.char * emissions';
downstream_impact = impact(impact_index, meta.Zdim+1:meta.Zdim+company.Dims);

fprintf("Compiling and saving reuslts.\n")
% Create table for results
direct_impact_results = [company.industries.company_industry'; num2cell(direct_impact)];
upstream_impact_results = [company.industries.company_industry'; num2cell(upstream_impact)];
downstream_impact_results = [company.industries.company_industry'; num2cell(downstream_impact)];
results = table(direct_impact_results, upstream_impact_results, downstream_impact_results);

warning('off', 'MATLAB:MKDIR:DirectoryExists');
mkdir("data\processed\")
writetable( ...
    results, ...
    "data\processed\results.xlsx", ...
    'Sheet', scenario, ...
    'Range', 'A1' ...
)
fprintf("Results succesfully saved for %s. \n \n", scenario)

end

